from django.views.generic.base import TemplateView


class JadoreView(TemplateView):
    template_name = "subscribe/jadore.html"
