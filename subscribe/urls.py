from django.conf.urls import url
from subscribe.views import JadoreView


urlpatterns = [
    url(r'^jadore/$', JadoreView.as_view(), name='jadore'),
]
