from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class PublishConfig(AppConfig):
    name = 'publish'
    verbose_name = _('publish')
