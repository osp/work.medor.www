from django import template

import HTMLParser
from publish.utils import typogrify as typogrify_util
from publish.models import Issue

from django.contrib.staticfiles.finders import find as find_static_file
from django.conf import settings

from django.core.files.storage import default_storage
import os.path


register = template.Library()


@register.filter(name='times')
def times(number):
    return range(1, number + 1)


@register.filter(name='decode_entities')
def decode_entities(text):
    html_parser = HTMLParser.HTMLParser()
    return html_parser.unescape(text)


@register.filter(name='typogrify')
def typogrify(html):
    return typogrify_util(html)


@register.inclusion_tag('publish/partials/pile-de-medors.html')
def pile_de_medor():
    issue = Issue.objects.order_by("-id").filter(published_online=True).first()
    return {'issue': issue}


# https://gist.github.com/goldhand/2f4bc7b1bb2cba76919b
@register.simple_tag
def encode_media(path, encoding='base64', file_type='image'):
    """
    a template tag that returns a encoded string representation of a mediafile
    Usage::
        {% encode_media path [encoding] %}
    Examples::
        <img src="{% encode_media 'path/to/img.png' %}">
    """
    if path.startswith(settings.MEDIA_URL):
        # remove MEDIA_URL if its included in the path
        path = path.replace(settings.MEDIA_URL, '')

    with default_storage.open(path) as f:
        data = f.read()

    ext = path.split('.')[-1]
    file_str = data.encode(encoding)

    return u"data:{0}/{1};{2},{3}".format(file_type, ext, encoding, file_str)
